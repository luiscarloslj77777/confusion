$(document).ready(function(){
    $("#mycarousel").carousel( { interval: 2000 } );
    $("#carouselButton").click(function(){
            if ($("#carouselButton").children("span").hasClass('fa-pause')) {
                $("#mycarousel").carousel('pause');
                $("#carouselButton").children("span").removeClass('fa-pause');
                $("#carouselButton").children("span").addClass('fa-play');
            }
            else if ($("#carouselButton").children("span").hasClass('fa-play')){
                $("#mycarousel").carousel('cycle');
                $("#carouselButton").children("span").removeClass('fa-play');
                $("#carouselButton").children("span").addClass('fa-pause');                    
            }
        });
});
$("#reservetablebtn").click(function(){
    $("#ReserveTableModal").modal('show');
});
$("#xbtnclose").click(function(){
    $("#ReserveTableModal").modal('hide');
});
$("#btncancelclose").click(function(){
    $("#ReserveTableModal").modal('hide');
});
$("#openloginbt").click(function(){
    $("#loginModal").modal('show');
});
$("#btnloginclose").click(function(){
    $("#loginModal").modal('hide');
});
$("#btncancellogin").click(function(){
    $("#loginModal").modal('hide');
});